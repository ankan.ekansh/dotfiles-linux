#!/bin/sh
# HISTFILE="$XDG_DATA_HOME"/zsh/history
HISTSIZE=1000000
SAVEHIST=1000000
export MANPAGER="most"
export PATH="$HOME/.local/bin":$PATH
export PATH=$HOME/.cargo/bin:$PATH
export PATH=$XDG_DATA_HOME/fnm:$PATH
export PATH="$PATH:./node_modules/.bin"
export GRIPHOME="$XDG_CONFIG_HOME/grip/"

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
eval "$(fnm env)"
eval "$(zoxide init zsh)"
# export PATH="$PATH:/home/ishaan/Files/Java/kafka-examples/kafka-server/bin" # kafka
# eval "`pip completion --zsh`"



# >>> conda initialize >>>
export CONDA_ROOT=$HOME/.local/miniconda3
__conda_setup="$('$CONDA_ROOT/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$CONDA_ROOT/etc/profile.d/conda.sh" ]; then
        . "$CONDA_ROOT/etc/profile.d/conda.sh"
    else
        export PATH="$CONDA_ROOT/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
