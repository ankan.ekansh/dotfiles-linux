#!/bin/sh
alias j='z'
alias f='zi'
alias g='git'

# mac like copy paste commands
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

alias zsh-update-plugins="find "$ZDOTDIR/plugins" -type d -exec test -e '{}/.git' ';' -print0 | xargs -I {} -0 git -C {} pull -q"

# from old zsh config
alias kitty-conf="$EDITOR $XDG_CONFIG_HOME/kitty/kitty.conf"
alias ccat="batcat --paging=never"
alias open="xdg-open "


alias new-venv="python3 -m venv .venv"
alias act-venv="source .venv/bin/activate"
alias del-venv="rm -rf .venv"

alias myip='curl http://ipecho.net/plain; echo'
alias hdd="/mnt/2CCA1EC4CA1E8A6A"

# ls mods
alias ls="exa --classify --group-directories-first"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# easier to read disk
alias df='df -h'     # human-readable sizes
alias free='free -m' # show sizes in MB

# get top process eating memory
alias psmem='ps auxf | sort -nr -k 4 | head -5'

# get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3 | head -5'

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"

# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# os specfic settings
# case "$(uname -s)" in
	# Darwin)
		# # echo 'Mac OS X'
		# alias ls='ls -G'
		# ;;
# 
	# Linux)
		# alias ls='ls --color=auto'
		# ;;
# 	
	# CYGWIN* | MINGW32* | MSYS* | MINGW*)
		# # echo 'MS Windows'
		# ;;
	# *)
		# # echo 'Other OS'
		# ;;
# esac
