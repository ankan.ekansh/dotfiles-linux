
## Linux Dotfiles

This Dotfile configuration has been done using `GNU Stow`. Some basic `Stow` Commands. Use the `-n` flag first to do a dry run and see what changes will be done to the system.

1. Move all the configuration files from the current directory (`dotfiles/stow/`) to the locations specified by the config.

    ```sh
    stow -v -t ~ -S */ 
    ```

2. Move a specific config (for example, just `zsh`) to its requied location

    ```sh
    stow -v -t ~ -S zsh/ 
    ```

3. Adopt a new file into stow config. (Create req. directory structure inside a folder in the stow root named with the logical group name of the config first). For example, like an `alacritty` folder .

    ```sh
    stow --adopt -v -t ~ -S alacritty/  
    ```
