export STARSHIP_CONFIG=$XDG_CONFIG_HOME/starship/starship.toml

function _precmd() {
    # Print a newline before the prompt, unless it's the
    # first prompt in the process.
    if [ -z "$NEW_LINE_BEFORE_PROMPT" ]; then
        NEW_LINE_BEFORE_PROMPT=1
    elif [ "$NEW_LINE_BEFORE_PROMPT" -eq 1 ]; then
        echo ""
    fi

    # set window title
    # echo -ne "\033]0; $(basename "$PWD") \007"
    # echo -ne "\033]0; $(short_pwd) \007"
    echo -ne "\033]0; $(prompt_pwd) \007"

}

precmd_functions+=(_precmd)

# last line
eval "$(starship init zsh)"
